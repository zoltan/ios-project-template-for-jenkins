//
//  AppDelegate.h
//  JenkinsDemo
//
//  Created by Zoltan Ulrich on 11.12.2013.
//  Copyright (c) 2013 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
