//
//  main.m
//  JenkinsDemo
//
//  Created by Zoltan Ulrich on 11.12.2013.
//  Copyright (c) 2013 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
