//
//  JenkinsDemoTests.m
//  JenkinsDemoTests
//
//  Created by Zoltan Ulrich on 11.12.2013.
//  Copyright (c) 2013 iQuest Technologies. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface JenkinsDemoTests : XCTestCase

@end

@implementation JenkinsDemoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testTheVerse
{
#define Da_ul_vostru YES
#define să_însemne ==
#define DA YES
    
    XCTAssertTrue(Da_ul_vostru să_însemne DA, @"Are you saying that Matthew 5:37 is wrong!?");
    
#undef Da_ul_vostru
#undef să_însemne
#undef DA
}

@end
